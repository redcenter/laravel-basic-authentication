<?php

namespace LaravelBasicAuth;

use Closure;
use Illuminate\Contracts\Routing\ResponseFactory as Response;
use Illuminate\Http\Request;
use Illuminate\Config\Repository as Config;

/**
 * Class LaravelBasicAuth
 * @package LaravelBasicAuth
 */
class LaravelBasicAuth
{
    /**
     * @var Config Container for injection
     */
    private $config;

    /**
     * @var Response
     */
    private $response;

    /**
     * LaravelBasicAuth constructor.
     *
     * @param Config $config
     * @param Response $response
     */
    public function __construct(Config $config, Response $response)
    {
        $this->config = $config;
        $this->response = $response;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $correctUsername = $this->config->get('app.basic_auth_user', null);
        $correctPassword = $this->config->get('app.basic_auth_password', null);

        $doWeNeedToCheck = !empty($correctUsername) && !empty($correctPassword);

        if($doWeNeedToCheck) {
            header('Cache-Control: no-cache, must-revalidate, max-age=0');
            $usernameSuppliedEarlier = $_SERVER['PHP_AUTH_USER'] ?? null;
            $passwordSuppliedEarlier = $_SERVER['PHP_AUTH_PW'] ?? null;

            $earlierCredentialsAreEmpty = empty($usernameSuppliedEarlier) || empty($passwordSuppliedEarlier);
            $earlierCredentialsAreIncorrect = $usernameSuppliedEarlier !== $correctUsername || $passwordSuppliedEarlier !== $correctPassword;

            if ($earlierCredentialsAreEmpty || $earlierCredentialsAreIncorrect) {
                $headers = [
                    'WWW-Authenticate' => 'Basic realm="Access denied"'
                ];
                return $this->response->make('', 401, $headers);
            }
        }

        return $next($request);
    }
}
