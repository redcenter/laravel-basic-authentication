<?php

use Illuminate\Http\Request;
use LaravelBasicAuth\LaravelBasicAuth;
use Symfony\Component\HttpFoundation\HeaderBag;
use Faker\Factory;
use Illuminate\Config\Repository as Config;
use Illuminate\Contracts\Routing\ResponseFactory as Response;

beforeEach(function () {
    $this->faker = Factory::create();
    $this->mocks['Config'] = Mockery::mock(Config::class);
    $this->mocks['Response'] = Mockery::mock(Response::class);
    $this->class = new LaravelBasicAuth($this->mocks['Config'], $this->mocks['Response']);
    $this->mocks['Request'] = \Mockery::mock(Request::class);
    $this->mocks['HeaderBag'] = \Mockery::mock(HeaderBag::class);
    $randomOutput = $this->faker->sentence;
    $this->mocks['Closure'] = function () use ($randomOutput) {return $randomOutput;};
});

/**
 * @covers LaravelBasicAuth
 */
it ('does not ask credentials when config is empty', function ($envUsername, $envPassword) {
    /**
     * Arrange
     */
    $this->mocks['Config']
        ->shouldReceive('get')
        ->once()
        ->withArgs(['app.basic_auth_user', null])
        ->andReturn($envUsername);

    $this->mocks['Config']
        ->shouldReceive('get')
        ->once()
        ->withArgs(['app.basic_auth_password', null])
        ->andReturn($envPassword);

    $expectedReturn = $this->mocks['Closure']();

    /**
     * Act
     */
    $actualReturn = $this->class->handle($this->mocks['Request'], $this->mocks['Closure']);

    /**
     * Assert
     */
    $this->assertSame($expectedReturn, $actualReturn);
})->with([
    ['no env variables' => null, null],
    ['empty env variables' => '', ''],
    ['password in env is empty' => 'username', null],
    ['username in env is empty' => null, 'password'],
]);

/**
 * @covers LaravelBasicAuth
 */
it ('does ask credentials when config is set', function () {
    /**
     * Arrange
     */
    $username = $this->faker->userName;
    $password = $this->faker->password;
    $fakeReturn = $this->faker->sentence;
    $expectedHeaders = ['WWW-Authenticate' => 'Basic realm="Access denied"'];

    $this->mocks['Config']
        ->shouldReceive('get')
        ->once()
        ->withArgs(['app.basic_auth_user', null])
        ->andReturn($username);

    $this->mocks['Config']
        ->shouldReceive('get')
        ->once()
        ->withArgs(['app.basic_auth_password', null])
        ->andReturn($password);

    $this->mocks['Response']
        ->shouldReceive('make')
        ->once()
        ->withArgs(['', 401, $expectedHeaders])
        ->andReturn($fakeReturn);

    $expectedReturn = $fakeReturn;

    /**
     * Act
     */
    $actualReturn = $this->class->handle($this->mocks['Request'], $this->mocks['Closure']);

    /**
     * Assert
     */
    $this->assertSame($expectedReturn, $actualReturn);
});

/**
 * @covers LaravelBasicAuth
 */
it ('does ask credentials when supplied credentials are bad', function () {
    /**
     * Arrange
     */
    $username = $this->faker->userName;
    $password = $this->faker->password;
    $_SERVER['PHP_AUTH_USER'] = $this->faker->userName;
    $_SERVER['PHP_AUTH_PW'] = $this->faker->userName;

    $expectedReturn = $this->faker->sentence;
    $expectedHeaders = ['WWW-Authenticate' => 'Basic realm="Access denied"'];

    $this->mocks['Config']
        ->shouldReceive('get')
        ->once()
        ->withArgs(['app.basic_auth_user', null])
        ->andReturn($username);

    $this->mocks['Config']
        ->shouldReceive('get')
        ->once()
        ->withArgs(['app.basic_auth_password', null])
        ->andReturn($password);

    $this->mocks['Response']
        ->shouldReceive('make')
        ->once()
        ->withArgs(['', 401, $expectedHeaders])
        ->andReturn($expectedReturn);

    /**
     * Act
     */
    $actualReturn = $this->class->handle($this->mocks['Request'], $this->mocks['Closure']);

    /**
     * Assert
     */
    $this->assertSame($expectedReturn, $actualReturn);
});
